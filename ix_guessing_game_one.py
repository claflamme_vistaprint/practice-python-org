"""
Generate a random number between 1 and 9 (including 1 and 9). 
Ask the user to guess the number, then tell them whether they guessed too low, too high, or exactly right. 
(Hint: remember to use the user input lessons from the very first exercise)

Extras:
    Keep the game going until the user types “exit”
    Keep track of how many guesses the user has taken, and when the game ends, print this out.
"""
import random


num_one = 1
num_two = 9


def get_user_guess(from_number, to_number):
    guess = input("Guess a number from {} to {}:\n".format(from_number, to_number))

    return int(guess)


def generate_random_number(from_number, to_number):
    rand_num = random.randint(from_number, to_number)

    return rand_num


def compare_guessed_number(actual_number, guessed_number):
    if actual_number == guessed_number:
        return "Congradulations. You guessed {}, which is correct." \
                .format(str(actual_number))
    elif actual_number < guessed_number:
        return "Sorry. Too high. You guessed {} but it was {}." \
                .format(str(guessed_number), str(actual_number))
    elif actual_number > guessed_number:
        return "Sorry. Too low. You guessed {} but it was {}." \
                .format(str(guessed_number), str(actual_number))
    else:
        print(actual_number, guessed_number)


def main():
    print(compare_guessed_number(generate_random_number(num_one, num_two), get_user_guess(num_one, num_two)))


if __name__ == "__main__":
    main()
