"""
Make a two-player Rock-Paper-Scissors game.
(Hint: Ask for player plays (using input), compare them,
print out a message of congratulations to the winner, and
ask if the players want to start a new game)

Remember the rules:
    Rock beats scissors
    Scissors beats paper
    Paper beats rock
"""
# Example while loop:
# while True:
#     usr_command = input("Enter your command: ")
#     if usr_command == "quit":
#         break
# else:
#     print("You typed " + usr_command)

# while True:
#     player_one_choice = input("Player One: Enter Rock, Paper, or Scissors:\n")
#     if player_one_choice.lower() == "rock" or "paper" or "scissors":
#         break
#     else:
#         break


def validate_input(player="Player One"):
    valid_choices = ["rock", "paper", "scissors"]
    choice = input("{}: Choose Rock, Paper, or Scissors:\n".format(
                                                            player))
    choice = choice.lower()

    while choice not in valid_choices:
        choice = input("Invalid selection. Try again:\n")

    return choice


def play_game():
    player_one_choice = validate_input("Player One")
    player_two_choice = validate_input("Player Two")

    if player_one_choice == "rock":
        if player_two_choice == "rock":
            print("Tie")
        elif player_two_choice == "paper":
            print("Player Two wins. Paper covers Rock.")
        elif player_two_choice == "scissors":
            print("Player One wins. Rock smashes Scissors.")
    elif player_one_choice == "paper":
        if player_two_choice == "rock":
            print("Player Two wins. Paper covers Rock.")
        elif player_two_choice == "paper":
            print("Tie.")
        elif player_two_choice == "scissors":
            print("Player Two wins. Scissors cut Paper.")
    elif player_one_choice == "scissors":
        if player_two_choice == "rock":
            print("Player Two wins. Rock smashes Scissors.")
        elif player_two_choice == "paper":
            print("Player One wins. Scissors cut Paper")
        elif player_two_choice == "scissors":
            print("Tie.")


play_game()
