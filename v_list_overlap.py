"""
List Overlap
Exercise 5 (and Solution)
Take two lists, say for example these two:

  a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
  b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
and write a program that returns a list that contains only the elements that
are common between the lists (without duplicates). Make sure your program
works on two lists of different sizes.

Extras:

Randomly generate two lists to test this
Write this in one line of Python (don’t worry if you can’t figure this out at
this point - we’ll get to it soon)
"""
import random

list_a = []
list_b = []
list_c = []

# randomly generate a range
list_a_length = random.randint(1, 300)
# randomly generate a number
for i in range(list_a_length):
    # append that number to list_a
    list_a.append(random.randint(0, 100))

# randomly generate a range
list_b_length = random.randint(1, 300)
# randomly generate a number
for i in range(list_b_length):
    # append that number to list_a
    list_b.append(random.randint(0, 100))

for i in list_a:
    if i in list_b and i not in list_c:
        list_c.append(i)

print(list_a.sort())
print(list_b.sort)
print(list_c.sort)
