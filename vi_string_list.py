"""
Ask the user for a string and print out whether this string is a palindrome or not.
(A palindrome is a string that reads the same forwards and backwards.)
"""


# set a new var with input.reverse() or something similar
def check_if_palindrome():
    user_input = input("Enter a string:\n").lower()
    reversed_input = user_input[len(user_input)::-1]

    if user_input == reversed_input:
        print("Yep. Palindrome")
    else:
        print("Nope. Not.")


check_if_palindrome()
