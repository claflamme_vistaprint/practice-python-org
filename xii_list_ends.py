"""
Write a program that takes a list of numbers (for example, a = [5, 10, 15, 20, 25])
and makes a new list of only the first and last elements of the given list.
For practice, write this code inside a function.
"""
import random


def main():
    usr_list = generate_list(int(input("How long is your list?\n")))
    new_list = refine_list(usr_list)

    print(usr_list)
    print(new_list)


def generate_list(usr_input):
    some_list = random.sample(
        range(0, 99),
        int(usr_input)
    )

    return some_list


def refine_list(usr_list):
    refined_list = []
    refined_list.append(usr_list[0])
    refined_list.append(usr_list[-1])

    return refined_list


if __name__ == "__main__":
    main()
