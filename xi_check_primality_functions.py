"""
Ask the user for a number and determine whether the number is prime or not.
(For those who have forgotten, a prime number is a number that has no divisors.).
You can (and should!) use your answer to Exercise 4 to help you.
Take this opportunity to practice using functions, described below.
"""


def main():
    number = user_input()
    print(evaluate_if_prime(number))


def user_input():
    user_num = int(input("Enter a number:\n"))

    return user_num


def evaluate_if_prime(num):
    divisors = []
    for i in range(2, num):
        if num % i == 0:
            divisors.append(i)
    if len(divisors) > 0:
        return "{} is totes not prime.".format(num)
    else:
        return "{} is totes prime.".format(num)


if __name__ == "__main__":
    main()
