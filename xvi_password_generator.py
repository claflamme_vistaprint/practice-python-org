# Write a password generator in Python.
# Be creative with how you generate passwords
# - strong passwords have a mix of lowercase letters, uppercase letters,
# numbers, and symbols.
# The passwords should be random, generating a new password every time the
# user asks for a new password. Include your run-time code in a main method.

#     Extra:
#         Ask the user how strong they want their password to be.
#         For weak passwords, pick a word or two from a list.
import random
import string


def main():
    password  = construct_password(get_password_length(),
                                   get_allowable_characters()
                                  )
                        
    print(password)


def get_password_length():
    desired_password_length = input("How many characters would you like your "
                                 "password to be? Ex. 18\n")
                            
    return int(desired_password_length)


def get_allowable_characters():
    letters = string.ascii_letters
    numbers = string.digits
    punctuation = string.punctuation
    allowable_characters = letters + numbers + punctuation

    return allowable_characters


def construct_password(desired_lenth, allowable_chars):
    password = []
    num_allowable_chars = len(allowable_chars)
    for i in range(0, (desired_lenth)):
        password.append(allowable_chars[random.randint(0,
                                                       (num_allowable_chars-1))])

    password = "".join(password)

    return password

if __name__ == "__main__":
    main()
