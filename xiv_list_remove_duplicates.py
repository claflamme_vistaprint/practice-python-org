'''
Write a program (function!) that takes a list and returns a new list 
that contains all the elements of the first list minus all the duplicates.

Extras:
Write two different functions to do this - one using a loop and constructing a list, and another using sets.
Go back and do Exercise 5 using sets, and write the solution for that in a different function.
'''
import random


def generate_random_list(smallest_num, largest_num):
    list_length = random.randint(smallest_num, largest_num)
    generated_list = list()

    for i in range(list_length):
        generated_list.append(random.randint(smallest_num, largest_num))

    return generated_list


def remove_duplicates(list_name):
    no_duplicates = list(set(list_name))

    return no_duplicates


first_list = generate_random_list(1, 40)
no_dupes = remove_duplicates(first_list)
print(sorted(no_dupes))