'''
Write a program that asks the user how many Fibonnaci numbers to generate and then generates them. 
Take this opportunity to think about how you can use functions. 
Make sure to ask the user to enter the number of numbers in the sequence to generate.
    (Hint: The Fibonnaci seqence is a sequence of numbers where the next number 
     in the sequence is the sum of the previous two numbers in the sequence. 
     The sequence looks like this: 1, 1, 2, 3, 5, 8, 13, …)
'''


def main():
    print(generate_fibonnaci_list(set_fib_num()))


def set_fib_num():
    fib_num = 0

    while fib_num < 3:
        fib_num = int(input("How many Fibonnaci numbers to generate?\n"))

    return fib_num


def generate_fibonnaci_list(list_length):
    fib_list = [1, 1]

    for i in range(0, list_length):
        fib_list.append(fib_list[i] + fib_list[i + 1])

    return fib_list


if __name__ == "__main__":
    main()
