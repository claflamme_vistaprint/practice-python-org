# Write a program(using functions!) that asks the user for
# a long string containing multiple words.
# Print back to the user the same string,
# except with the words in backwards order.

# For example, say I type the string:

#   My name is Michele
# Then I would see the string:

#   Michele is name My
# shown back to me.

def get_user_input():
    user_input = input("Enter a long string containing multiple words:\n")

    return user_input


def reverse_list(words):
    list_of_words = words.split(" ")
    list_of_words.reverse()
    reversed_words = " ".join(list_of_words)
    
    return reversed_words

    
print(reverse_list(get_user_input()))

# more efficient one liner
def reverse_words(words):
    return ' '.join(words.split()[::-1])
