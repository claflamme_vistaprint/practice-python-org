from pprint import pprint as pp

"""
Let’s say I give you a list saved in a variable:
    a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100].
Write one line of Python that takes this list a and
makes a new list that has only the even elements of this list in it.
"""

# List Comprehension Examples:
# years_of_birth = [1990, 1991, 1990, 1990, 1992, 1991]
# ages = [2019 - year for year in years_of_birth]

# pp(ages)

# s = "this is a longish string"

# letters = [l for l in s]
# pp(letters)

a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

b = [n for n in a if n % 2 == 0]
print(b)
